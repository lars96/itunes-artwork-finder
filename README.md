iTunes Artwork Finder
=====================

This is a fork of the original repository at [https://bendodson.com/projects/itunes-artwork-finder/](https://bendodson.com/projects/itunes-artwork-finder/).

Apart from tiny changes, the service allows to directly access cover arts with a resolution of 1000 by 1000 pixels.
Minor changes include reorganization of drop-down entries ('Album' and 'Germany' first) as well as the renamed link to 'Very high resolution'.

A demo page can be found [here](https://larsbutnotleast.xyz/artwork/).